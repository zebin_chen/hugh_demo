package org.byron.service.Impl;

import java.util.List;

import org.byron.dao.UserInfoMapper;
import org.byron.model.UserInfo;
import org.byron.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 创建时间：2015-9-8 下午5:22:59
 * 
 * @author byron
 * @version 2.2
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Override
	public UserInfo getUserById(int id) {
		return userInfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<UserInfo> getUsers() {
		// return userInfoMapper.selectAll();
		return null;
	}

	@Override
	public int insert(UserInfo userInfo) {

		int result = userInfoMapper.insert(userInfo);

		System.out.println(result);
		return result;
	}

}